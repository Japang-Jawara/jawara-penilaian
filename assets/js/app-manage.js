$(document).ready(function() {

    if ($('#tbl-jawara').length !== 0) {
        var url = 'jawara/tableJawaraLeds';
        $('#tbl-jawara').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }
});