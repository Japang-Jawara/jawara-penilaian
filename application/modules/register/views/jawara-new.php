	<section class="page-section" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Selamat ! Selangkah lebih dekat lagi untuk memiliki bisnis dengan dimodalin oleh Jaring Pangan Indonesia.</p>
			</div>
            <!-- begin wizard-form -->
			<form action="index.php/register/jawara/penilaian" enctype="multipart/form-data" method="POST">    
				<div class="row">
					<input type="hidden" name="autocomplete" id="field-autocomplete">
					<input class="form-control" type="hidden" name="id" id="idx" />

					<div class="form-group col-xl-12">
						<label class="col-form-label">Email Jawara <span class="text-danger">*</span></label>
						<input type="email" class="form-control" name="email" id="email" placeholder="Email Jawara" autocomplete="off" required>
					</div>

					<div class="form-group col-xl-12">
						<label class="col-form-label">Nama Toko <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="shop_name" id="shop_name" placeholder="Nama Toko" autocomplete="off" required>
					</div>

					<div class="form-group col-xl-12">
						<label class="col-form-label">Lokasi (Alamat Toko): <span class="text-danger">*</span></label>
						<textarea class="form-control" name="shop_location" id="shop_location" required></textarea>
					</div>

					<div class="form-group col-xl-12">
						<h2>Social Aspects</h2>
						<br>
						<h5>Administration Score</h5>
						<br>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Pengalaman Berdagang <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getSAA as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="sa_pengalaman" id="sa_pengalaman" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
					</div>

					<div class="form-group col-xl-12">
						<h5>Behavior and Personality Score</h5>
						<br>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Jumlah Karyawan <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getSABPJKA as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="sa_karyawan" id="sa_karyawan" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Jumlah Keluarga yang menjadi tanggungan <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getSABPJKE as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="sa_keluarga" id="sa_keluarga" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Aktif dalam kegiatan lingkungan RT/RW/Tempat Ibadah dll <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getSABPA as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="sa_aktif" id="sa_aktif" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
					</div>

					<div class="form-group col-xl-12">
						<h2>Business Aspects</h2>
						<p>review of personal capabilities based on business experience</p>
						<br>
						<h5>Business Experience</h5>
						<br>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Jenis Usaha <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getBALJU as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="ba_jusaha" id="ba_jusaha" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Modal Usaha diluar JAWARA <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getBALMU as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="ba_musaha" id="ba_musaha" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
					</div>

					<div class="form-group col-xl-12">
						<h2>Commercial Aspects</h2>
						<p>Business location and environment that supports business openings</p>
						<br>
						<h5>Market Scoring</h5>
						<br>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Posisi Lokasi <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getCAMPL as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="ca_location" id="ca_location" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Kompetitor sejenis di area <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getCAMKS as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="ca_kompetitor" id="ca_kompetitor" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
					</div>

					<div class="form-group col-xl-12">
						<h2>Financial and Risk Management Aspects</h2>
						<br>
						<h5>Personal Financial Scoring</h5>
						<br>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Kepemilikan Aset Pribadi <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getFRMAIKA as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="frma_aset" id="frma_aset" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
						<fieldset class="form-group">
							<div class="row">
								<legend class="col-form-label col-sm-4 pt-0">Memiliki Pendapatan lain diluar berdagang (Gaji/investasi) <span class="text-danger">*</span></legend>
								<div class="col-sm-8">
										
									<?php foreach ($getFRMAIMP as $r) { ?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="frma_pendapatan" id="frma_pendapatan" value="<?= $r->status; ?>" required>
										<label class="form-check-label">
											<?= $r->ket; ?>
										</label>
									</div>
												
									<?php } ?>
								</div>      
							</div>         
						</fieldset>
					</div>

					<div class="form-group col-xl-12">
						<!-- begin form-group -->
						<h5>Selfie Jawara</h5>
						<br>
						<div class="form-group row m-b-10">
							<label class="col-lg-3 text-lg-right col-form-label">Selfie Jawara <span class="text-danger">*</span></label>
							<div class="col-lg-9 col-xl-6">
								<input type="file" name="file_selfie" accept="image/png, image/jpg, image/jpeg" class="form-control" required/>
								<span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
							</div>
						</div>
						<!-- end form-group -->
					</div>
					

					<div class="form-group col-xl-12">
						<label class="col-form-label">Additional Notes to highlight:</label>
						<textarea class="form-control" name="notes" id="notes"></textarea>
					</div>                    
				</div>
				
				</br>
				<div class="modal-footer">
					<button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
					<button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
			<!-- end wizard-form -->
			<br><br>
		</div>
		<br><br><br>
	</section>

	<section class="page-section" id="jawara">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">Apa Itu JaWaRa?</h4>
				<p class="section-subheading text-muted"><b>JaWaRa atau Japang Warung Rakyat</b> adalah sebuah strategi JaPang untuk membantu para UMKM dan individu yang ingin memiliki usaha namun memiliki keterbatasan modal.</p>
			</div>
			<div class="text-center">
				<img class="img-fluid" src="<?php echo base_url('assets/img/gallery/jawara.png'); ?>" width="800px"/>
			</div>
			<div class="text-center">
				<img class="img-fluid" src="<?php echo base_url('assets/img/gallery/flow.png'); ?>" width="1000px"/>
			</div>
			<div class="text-center">
				<img class="img-fluid" src="<?php echo base_url('assets/img/gallery/syarat.jpg'); ?>" width="1000px"/>
			</div>
		</div>
		<br><br><br><br><br>
	</section>

	<section class="page-section" id="product">
		<div class="container">
			<div class="text-center">
				<p class="section-subheading text-muted judul">Our Product</p>
            </div>
			<div class="row text-center">
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/beras.png'); ?>" width="50%">
					</span>
					<h5 class="monst">RICE</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/telur.png'); ?>" width="50%">
					</span>
					<h5 class="monst">EGG</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/daging.png'); ?>" width="50%">
					</span>
					<h5 class="monst">MEAT</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/minyak.png'); ?>" width="50%">
					</span>
					<h5 class="monst">GROCERIES</h5>
				</div>
			</div>
		</div>
		<br><br><br><br><br>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="termsconditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Terms dan Conditions</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">dantimes;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						<b>Bagi para calon jawara:</b>
					</p>
					<p>- Proses ini tidak dikenakan biaya tambahan apapun.</p>
					<p>- Bagi para jawara yang sudah lolos seleksi dan pantas untuk mendapatkan pendanaan, jika tidak melanjutkan proses (tidak dapat dihubungi, pengunduran diri, dan lainnya) akan berdampak pada sistem blacklist untuk seluruh anggota keluarga dalam kartu keluarga sehingga tidak dapat mengajukan di kedepan harinya.</p>
					<p>- Pengunduran diri setelah penandatanganan akan dikenakan biaya penalty 5% dari total nilai pengajuan dana.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
