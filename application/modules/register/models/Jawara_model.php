<?php
class Jawara_model extends CI_Model{

    public function cekEmail($email){
        return $this->db->where('email', $email)->limit(1)->get('jawara_leads')->result();
    }

    public function penilaian($data){

        $cekEmail = $this->cekEmail($data['email_jawara_sc']);

        if($cekEmail != NULL){
            $data['jawara_id'] = $cekEmail[0]->id;
            $data['nik_ktp_sc'] = $cekEmail[0]->nik_ktp;
            $data['name_sc'] = $cekEmail[0]->name;
            $data['phone_sc'] = $cekEmail[0]->phone;

            $this->db->insert('jawara_score', $data);
            $this->session->set_flashdata('success','PENILAIAN BERHASIL, MOHON MENUNGGU TINDAK LANJUTNYA DARI TEAM KAMI');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','REGISTRASI GAGAL, EMAIL YANDA ANDA MASUKKAN TIDAK ADA PADA DATABASE KAMI');
            return FALSE;
        }
    }

    // CONFIG
    public function getSAA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SAA')->get()->result();
    }

    public function getSABPJKA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SABPJKA')->get()->result();
    }
    
    public function getSABPJKE(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SABPJKE')->get()->result();
    }

    public function getSABPA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SABPA')->get()->result();
    }

    public function getBALJU(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BALJU')->get()->result();
    }

    public function getBALMU(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BALMU')->get()->result();
    }

    public function getCAMPL(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'CAMPL')->get()->result();
    }

    public function getCAMKS(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'CAMKS')->get()->result();
    }

    public function getFRMAIKA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'FRMAIKA')->get()->result();
    }

    public function getFRMAIMP(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'FRMAIMP')->get()->result();
    }

}
?>