<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawara extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Jawara_model', 'modelJawara');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
     }

     function uploadSelfie()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_selfie']['name'];
			$tmpname = $_FILES['file_selfie']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'SELFIE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/selfie/';
			$config['upload_url'] =  base_url() . 'assets/berkas/selfie/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/selfie/" . $newname);
			return $newname;
		}
	}

     function index(){

          $x['getSAA'] = $this->modelJawara->getSAA();
          $x['getSABPJKA'] = $this->modelJawara->getSABPJKA();
          $x['getSABPJKE'] = $this->modelJawara->getSABPJKE();
          $x['getSABPA'] = $this->modelJawara->getSABPA();
          $x['getBALJU'] = $this->modelJawara->getBALJU();
          $x['getBALMU'] = $this->modelJawara->getBALMU();
          $x['getCAMPL'] = $this->modelJawara->getCAMPL();
          $x['getCAMKS'] = $this->modelJawara->getCAMKS();
          $x['getFRMAIKA'] = $this->modelJawara->getFRMAIKA();
          $x['getFRMAIMP'] = $this->modelJawara->getFRMAIMP();

          // $this->load->view("include/frontend/head");
          // $this->load->view("include/frontend/header");
          // $this->load->view('jawara-new', $x);
          // $this->load->view("include/frontend/footer");
          // $this->load->view("include/alert");

          $this->load->view('../../views/404');
     }

     public function penilaian(){

          $this->form_validation->set_rules('email', 'Email', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {
               $file_selfie = '';

               $x['email_jawara_sc'] = $this->input->post('email');
               $x['nik_ktp_sc'] = $this->input->post('nik_ktp');
               $x['name_sc'] = $this->input->post('name');
               $x['phone_sc'] = $this->input->post('phone');
               $x['shop_name_sc'] = $this->input->post('shop_name');
               $x['shop_location_sc'] = $this->input->post('shop_location');

               $x['sa_pengalaman_sc'] = $this->input->post('sa_pengalaman');
               $x['sa_karyawan_sc'] = $this->input->post('sa_karyawan');
               $x['sa_keluarga_sc'] = $this->input->post('sa_keluarga');
               $x['sa_aktif_sc'] = $this->input->post('sa_aktif');

               $x['ba_jusaha_sc'] = $this->input->post('ba_jusaha');
               $x['ba_musaha_sc'] = $this->input->post('ba_musaha');

               $x['ca_location_sc'] = $this->input->post('ca_location');
               $x['ca_kompetitor_sc'] = $this->input->post('ca_kompetitor');

               $x['frma_aset_sc'] = $this->input->post('frma_aset');
               $x['frma_pendapatan_sc'] = $this->input->post('frma_pendapatan');

               $x['notes_sc'] = $this->input->post('notes');

               if (!empty($_FILES['file_selfie']['name'])) {
                    $newname = $this->uploadSelfie();
                    $data['file_selfie'] = $newname;
                    $file_selfie = $newname;

                    $x['file_selfie_sc'] = base_url('assets/berkas/selfie/').$file_selfie;
               }

               $ms = '2';

               $x['ts_sa_sc'] = ($x['sa_pengalaman_sc']*$ms) + ($x['sa_karyawan_sc']*$ms) + ($x['sa_keluarga_sc']*$ms) + ($x['sa_aktif_sc']*$ms);
               $x['ts_ba_sc'] = ($x['ba_jusaha_sc']*$ms) + ($x['ba_musaha_sc']*$ms);
               $x['ts_ca_sc'] = ($x['ca_location_sc']*$ms) + ($x['ca_kompetitor_sc']*$ms);
               $x['ts_frma_sc'] = ($x['frma_aset_sc']*$ms) + ($x['frma_pendapatan_sc']*$ms);

               // RUMUS
               $x['percent_sa_sc'] = round(($x['ts_sa_sc'] / 24 * 100));
               $x['percent_ba_sc'] = round(($x['ts_ba_sc'] / 12 * 100));
               $x['percent_ca_sc'] = round(($x['ts_ca_sc'] / 12 * 100));
               $x['percent_frma_sc'] = round(($x['ts_frma_sc'] / 18 * 100));

               $percent_sa = round(($x['ts_sa_sc'] / 24 * 100))/100;
               $percent_ba = round(($x['ts_ba_sc'] / 12 * 100))/100;
               $percent_ca = round(($x['ts_ca_sc'] / 12 * 100))/100;
               $percent_frma = round(($x['ts_frma_sc'] / 18 * 100))/100;

               
               $sa = '0.2';
               $ba = '0.4';
               $ca = '0.2';
               $frma = '0.2';

               $total = ($percent_sa*$sa) + ($percent_ba*$ba) + ($percent_ca*$ca) + ($percent_frma*$frma);
               $x['total_score_sc'] = round(($total * 100));

               $total_score = round(($total * 100));

               if($total_score >= 85){
                    $x['user_grading_sc'] = 'A+';
                    $x['status_score_sc'] = '1';
               }else if($total_score >= 70){
                    $x['user_grading_sc'] = 'A';
                    $x['status_score_sc'] = '1';
               }else if($total_score >= 60){
                    $x['user_grading_sc'] = 'B+';
                    $x['status_score_sc'] = '1';
               }else if($total_score >= 50){
                    $x['user_grading_sc'] = 'B';
                    $x['status_score_sc'] = '1';
               }else if($total_score >= 40){
                    $x['user_grading_sc'] = 'C';
                    $x['status_score_sc'] = '1';
               }else if($total_score >= 30){
                    $x['user_grading_sc'] = 'C-';
                    $x['status_score_sc'] = '0';
               }else if($total_score >= 15){
                    $x['user_grading_sc'] = 'D';
                    $x['status_score_sc'] = '0';
               }else{
                    $x['user_grading_sc'] = 'E';
                    $x['status_score_sc'] = '0';
               }
               
               $x['created_at_sc'] = date('Y-m-d h:i:s');

               $this->modelJawara->penilaian($x);

               redirect('');
          }

     }

}