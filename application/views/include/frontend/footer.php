    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
				<div class="col-lg-6">
					<h6 style="color:white">HUBUNGI KAMI</h6>
					<ul class="list-unstyled li-space-lg p-small">
						<li>+62 812 1010 3267</li>
						<li>sales@jaringpangan.com</li>
					</ul>
					<ul class="list-unstyled li-space-lg p-small">
						<li>Sovereign Plaza 12th floor, Unit E Jl. TB Simatupang No. 36, RT 1/RW 2 Cilandak, Jakarta Selatan 12430</li>
						<!-- <li>Karangjati MT I/304</li>
						<li>Sinduadi, Mlati, Sleman</li>
						<li>Yogyakarta 55284</li> -->
					</ul>
				</div>
				<div class="col-lg-3">
					<h6 style="color:white">Tentang Kami</h6>
					<ul class="list-unstyled li-space-lg p-small">
                        <li>
                            <a href="https://www.linkedin.com/company/jaring-pangan-indonesia"><i class="fab fa-linkedin" style="font-size:30px"></i></a>
						    <a href="https://www.facebook.com/Jaring-Pangan-Indonesia-101461872088976"><i class="fab fa-facebook " style="font-size:30px"></i></a>
                            <a href="https://www.instagram.com/jaringpangan/"><i class="fab fa-instagram" style="font-size:30px"></i></a>
                        </li>
					</ul>
				</div>
                <div class="col-lg-3">
					<h6 style="color:white">Lokasi</h6>
					<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=Jaring%20Pangan%20Indonesia&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near" title="Jaring Pangan Indonesia" aria-label="Jaring Pangan Indonesia"></iframe>
				</div>
                
            </div> <!-- end of row -->
			
        </div> <!-- end of container -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->



    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
			<div class="row">
				<div class="col-lg-9">
					<!-- <span class="float-left p-small" style="color: white; text-align: left;">
						Copyright &copy;<?= date('Y'); ?> JaPang Tech
					</span> -->
				</div>
				<div class="col-lg-3 p-small">
					<!-- <span class="float-right" style="color: white; text-align: right;">Syarat dan Ketentuan | Kebijakan Privasi</span> -->
                    <span class="float-right p-small" style="color: white; text-align: right;">
						Copyright &copy;<?= date('Y'); ?> JaPang Tech
					</span>
                </div>
			</div>
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    	
    <!-- Scripts -->
    <script src="<?php echo base_url('assets/frontend/custom/js/jquery.min.js'); ?>"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="<?php echo base_url('assets/frontend/custom/js/bootstrap.min.js'); ?>"></script> <!-- Bootstrap framework -->
	<script src="<?php echo base_url('assets/frontend/custom/js/scripts.js'); ?>"></script> <!-- Custom scripts -->

    <script src="<?php echo base_url('assets/js/app.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/theme/default.min.js');?>"></script>
	<!-- ================== END BASE JS ================== -->
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url('assets/plugins/datatables.net/js/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons/js/buttons.colVis.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons/js/buttons.flash.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/pdfmake/build/pdfmake.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/pdfmake/build/vfs_fonts.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jszip/dist/jszip.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/demo/table-manage-buttons.demo.js');?>"></script>

	<!-- v1 -->
	<script src="<?php echo base_url('assets/plugins/flot/jquery.flot.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/flot/jquery.flot.time.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/flot/jquery.flot.resize.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/flot/jquery.flot.pie.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jquery-sparkline/jquery.sparkline.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jvectormap-next/jquery-jvectormap.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jvectormap-next/jquery-jvectormap-world-mill.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js');?>"></script>
	<script src="<?php echo base_url('assets/js/demo/dashboard.js');?>"></script>

	<!-- icons  -->
	<script src="<?php echo base_url('assets/plugins/highlight.js/highlight.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/demo/render.highlight.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/select2/dist/js/select2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/moment/min/moment.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>

	<!-- <script src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script> -->
	<script src="<?php echo base_url('assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
	<script src="//cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>
	<script src="<?php echo base_url('assets/js/demo/form-wysiwyg.demo.js'); ?>"></script>

	<script src="<?php echo base_url('assets/plugins/parsleyjs/dist/parsley.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/smartwizard/dist/js/jquery.smartWizard.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/demo/form-wizards-validation.demo.js'); ?>"></script>

	<script src="<?php echo base_url('assets/plugins/d3/d3.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/nvd3/build/nv.d3.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/clipboard/dist/clipboard.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/demo/widget.demo.js'); ?>"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>