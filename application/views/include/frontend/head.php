<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Your description">
    <meta name="author" content="Niagahoster">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on Facebook, Twitter, LinkedIn -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta name="twitter:card" content="summary_large_image"> <!-- to have large image post format in Twitter -->
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"> -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <!-- Webpage Title -->
    <title>PENILAIAN JAWARA</title>

	<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">
    
    <!-- Styles -->
    <link href="<?php echo base_url('assets/frontend/custom/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/custom/css/swiper.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/custom/css/styles.css'); ?>" rel="stylesheet">

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url('assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet');?>" />
	
	<!-- v1 -->
	<link href="<?php echo base_url('assets/plugins/jvectormap-next/jquery-jvectormap.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/gritter/css/jquery.gritter.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/select2/dist/css/select2.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" />

	<link href="<?php echo base_url('assets/plugins/smartwizard/dist/css/smart_wizard.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/nvd3/build/nv.d3.css');?>" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	
	<!-- Favicon  -->
    <link rel="icon" href="<?php echo base_url('assets/img/logo/japang.png'); ?>">
</head>
<body data-spy="scroll" data-target=".fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <div class="container">
			
            <!-- Image Logo -->
            <a class="navbar-brand logo-image" href="#"><img src="<?php echo base_url('assets/img/logo/japang-logo.jpg'); ?>" alt="logo" style="width:180px; height:50px;"></a> 

            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#daftar">Penilaian Jawara</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#jawara">Tentang Jawara</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#product">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="https://jaringpangan.com" target="_blank">Japang</a>
                    </li>
                </ul>
            </div> <!-- end of navbar-collapse -->
        </div> <!-- end of container -->
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->